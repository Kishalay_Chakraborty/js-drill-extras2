function problem3(users) {
    let masters = [];

    for(let user in users) {
        if(users[user].qualification === "Masters") {
            masters.push(user);
        }
    }

    return masters;
}

module.exports = problem3;