function problem4(users) {
    let groupByLang = {};

    for(let user in users) {
        let designation = users[user].desgination;

        if(designation) {
            designation = designation.toLowerCase();

            if(designation.includes("golang")) {
                if(!groupByLang.Golang) {
                    groupByLang.Golang = [];
                }

                groupByLang.Golang.push(user);
            }
            else if(designation.includes("javascript")) {
                if(!groupByLang.Javascript) {
                    groupByLang.Javascript = [];
                }

                groupByLang.Javascript.push(user);
            }
            else if(designation.includes("python")) {
                if(!groupByLang.Python) {
                    groupByLang.Python = [];
                }

                groupByLang.Python.push(user);
            }
        }
    }

    return groupByLang;
}

module.exports = problem4;