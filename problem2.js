function problem2(users) {
    let inGer = [];

    for(let user in users) {
        if(users[user].nationality === "Germany") {
            inGer.push(user);
        }
    }

    return inGer;
}

module.exports = problem2;