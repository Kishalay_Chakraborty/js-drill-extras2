function problem1(users) {
    let gamelovers = [];

    for(let user in users) {
        if(users[user].interests[0].includes("Video Games")) {
            gamelovers.push(users[user]);
        }
    }

    return gamelovers;
}

module.exports = problem1;